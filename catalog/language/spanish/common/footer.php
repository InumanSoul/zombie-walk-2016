<?php
// Text
$_['text_information']  = 'Información';
$_['text_service']      = 'Servicio al cliente';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contáctenos';
$_['text_return']       = 'Devoluciones';
$_['text_sitemap']      = 'Mapa del sitio';
$_['text_manufacturer'] = 'Marcas';
$_['text_voucher']      = 'Vales de regalo';
$_['text_affiliate']    = 'Afiliados';
$_['text_special']      = 'Especiales';
$_['text_account']      = 'Mi Cuenta';
$_['text_order']        = 'Historial de órdenes';
$_['text_wishlist']     = 'Lista de Deseos';
$_['text_newsletter']   = 'Boletin';
$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
