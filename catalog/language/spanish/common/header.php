<?php
// Text
$_['text_home']          = 'Inicio';
$_['text_map']           = 'Mapa';
$_['text_rules']         = 'Reglamento';
$_['text_gallery']       = 'Galería';
$_['text_shop']          = 'Tienda';
$_['text_language']      = 'Idioma';
$_['text_wishlist']      = 'Lista de Deseos (%s)';
$_['text_shopping_cart'] = 'Carro de compras';
$_['text_category']      = 'Categorías';
$_['text_account']       = 'Mi cuenta';
$_['text_register']      = 'Registrarse';
$_['text_login']         = 'Ingresar';
$_['text_order']         = 'Historial de Ordenes';
$_['text_transaction']   = 'Transacciones';
$_['text_download']      = 'Descargas';
$_['text_logout']        = 'Salir';
$_['text_checkout']      = 'Pagar';
$_['text_search']        = 'Buscar';
$_['text_all']           = 'Ver Todo';