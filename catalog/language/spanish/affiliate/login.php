<?php
// Heading
$_['heading_title']                 = 'Programa de Afiliado';

// Text
$_['text_account']                  = 'Cuenta';
$_['text_login']                    = 'Ingresar';
$_['text_description']              = '<p>%s el programa de afiliados es gratuito y permite a los miembros obtener ingresos mediante la colocación de un enlace o enlaces en su sitio web que anuncia %s o productos específicos. Las ventas realizadas a los clientes que han hecho clic en los enlaces le hará ganar la comisión de afiliado. La tasa de comisión estándar es actualmente %s.</p><p>Para obtener más información, visite nuestra Página de Preguntas Frecuentes o consulte nuestros términos y condiciones del programa de afiliados</p>';
$_['text_new_affiliate']            = 'Nuevo Afiliado';
$_['text_register_account']         = '<p>Yo no soy actualmente un afiliado.</p><p>Haga clic en Continuar para crear una nueva cuenta de afiliado. Tenga en cuenta que esto no está conectado de ninguna manera a su cuenta de cliente.</p>';
$_['text_returning_affiliate']      = 'Ingreso de Afiliado';
$_['text_i_am_returning_affiliate'] = 'Soy un afiliado que regresa.';
$_['text_forgotten']                = '¿Olvide mi contraseña?';

// Entry
$_['entry_email']                   = 'E-mail de afiliado';
$_['entry_password']                = 'Contraseña';

// Error
$_['error_login']                   = '¡Advertencia: No hay resultados para la dirección de e-mail y/o contraseña!';
$_['error_attempts']                = '¡Advertencia: Tu cuenta ha superado el número permitido de intentos de conexión. Inténtelo de nuevo en 1 hora!';
$_['error_approved']                = '¡Advertencia: Su cuenta requiere aprobación antes de que p
