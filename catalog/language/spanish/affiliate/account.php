<?php
// Heading
$_['heading_title']        = 'Cuenta de Afiliado';

// Text
$_['text_account']         = 'Cuenta';
$_['text_my_account']      = 'Mi cuenta de afiliado';
$_['text_my_tracking']     = 'Información de rastreo';
$_['text_my_transactions'] = 'Mis transacciones';
$_['text_edit']            = 'Editar información de su cuenta';
$_['text_password']        = 'Cambiar su contraseña';
$_['text_payment']         = 'Cambiar sus preferencias de pago';
$_['text_tracking']        = 'Personalizar código de rastreo de afiliado';
$_['text_transaction']     = 'Ver su historial de transacciones';
