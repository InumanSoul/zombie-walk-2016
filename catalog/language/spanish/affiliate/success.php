<?php
// Heading
$_['heading_title'] = '!Su cuenta de afiliado ha sido creada!';

// Text
$_['text_message']  = '<p>!Felicitaciones, su nueva cuenta ha sido creada con éxito!</p> <p>Ahora eres un miembro %s afiliado.</p> <p>Si usted tiene alguna pregunta acerca del funcionamiento de este sistema de afiliados, por favor envíe un correo electrónico al propietario de la tienda.</p> <p>Una confirmación ha sido enviado a la dirección de correo electrónico proporcionada. Si no lo ha recibido en una hora, por favor <a href="%s">contactenos</a>.</p>';
$_['text_approval'] = '<p>!Gracias por registrarse para una cuenta de afiliado con %s!</p><p>Se le notificará por correo electrónico una vez que su cuenta ha sido activada por el dueño de la tienda.</p><p>Si usted tiene alguna pregunta acerca del funcionamiento de este sistema de afiliados, por favor pongase <a href="%s">en contacto con el dueño de la tienda</a>.</p>';
$_['text_account']  = 'Cuenta';
$_['text_success']  = 'Exito';
