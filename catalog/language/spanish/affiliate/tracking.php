<?php
// Heading
$_['heading_title']    = 'Seguimiento del afiliado';

// Text
$_['text_account']     = 'Cuenta';
$_['text_description'] = 'Para asegurarse de que le pagan por referidos que nos envie, necesitamos hacer un seguimiento de la remisión mediante la colocación de un código de seguimiento en el vinculo de la URL. Puede utilizar las herramientas a continuación para generar enlaces con el %s sitio web.';

// Entry
$_['entry_code']       = 'Su código de seguimiento';
$_['entry_generator']  = 'Generador de enlace de seguimiento';
$_['entry_link']       = 'Enlace de seguimiento';

// Help
$_['help_generator']  = 'Escriba el nombre de un producto que desea enlazar';
