<?php
// Heading
$_['heading_title']             = 'Método de Pago';

// Text
$_['text_account']              = 'Cuenta';
$_['text_payment']              = 'Pago';
$_['text_your_payment']         = 'Información de Pago';
$_['text_your_password']        = 'Su contraseña';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Transferencia Bancaria';
$_['text_success']              = 'Exito: Su cuenta ha sido actualizada correctamente.';

// Entry
$_['entry_tax']                 = 'ID de impuesto';
$_['entry_payment']             = 'Metodo de Pago';
$_['entry_cheque']              = 'Cheque Payee **';
$_['entry_paypal']              = 'E-mail de Cuenta PayPal';
$_['entry_bank_name']           = 'Nombre del Banco';
$_['entry_bank_branch_number']  = 'Número ABA / BSB (Número de sucursal)';
$_['entry_bank_swift_code']     = 'Codigo SWIFT';
$_['entry_bank_account_name']   = 'Nombre de la cuenta';
$_['entry_bank_account_number'] = 'Número de Cuenta';
