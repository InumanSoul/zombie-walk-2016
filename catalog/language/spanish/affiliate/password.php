<?php
// Heading
$_['heading_title']  = 'Cambiar contraseña';

// Text
$_['text_account']   = 'Cuenta';
$_['text_password']  = 'Su contraseña';
$_['text_success']   = 'Exito: La contraseña se ha actualizado correctamente.';

// Entry
$_['entry_password'] = 'Contraseña';
$_['entry_confirm']  = 'Confirme contraseña';

// Error
$_['error_password'] = '¡La contraseña debe tener entre 4 y 20 caracteres!';
$_['error_confirm']  = '¡Confirmación de la contraseña no coincide con la contraseña!';
