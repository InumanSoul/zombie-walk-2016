<?php
// Heading
$_['heading_title']     = 'Información de Cuenta';

// Text
$_['text_account']      = 'Cuenta';
$_['text_edit']         = 'Editar información';
$_['text_your_details'] = 'Detalles personales';
$_['text_your_address'] = 'Dirección';
$_['text_success']      = 'Exito: su cuenta se ha modificado.';

// Entry
$_['entry_firstname']   = 'Nombre';
$_['entry_lastname']    = 'Apellido';
$_['entry_email']       = 'E-Mail';
$_['entry_telephone']   = 'Teléfono';
$_['entry_fax']         = 'Fax';
$_['entry_company']     = 'Empresa';
$_['entry_website']     = 'Sitio web';
$_['entry_address_1']   = 'Dirección 1';
$_['entry_address_2']   = 'Dirección 2';
$_['entry_postcode']    = 'Código Postal';
$_['entry_city']        = 'Ciudad';
$_['entry_country']     = 'Pais';
$_['entry_zone']        = 'Region / Estado';

// Error
$_['error_exists']      = '¡Advertencia: El e-mail ya está registrado!';
$_['error_firstname']   = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']    = 'El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']       = '¡El e-mail no parece ser válido!';
$_['error_telephone']   = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_address_1']   = '¡La dirección 1 debe tener entre 3 y 128 caracteres!';
$_['error_city']        = '¡La ciudad debe tener entre 2 y 128 caracteres!';
$_['error_country']     = '¡Por favor, seleccione un país!';
$_['error_zone']        = '¡Por favor seleccione una región / estado!';
$_['error_postcode']    = '¡El código postal debe tener entr