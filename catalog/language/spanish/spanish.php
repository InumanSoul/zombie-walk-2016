<?php
  // Locale
$_['code']                  = 'es';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = ',';
$_['thousand_point']        = '.';

// Text
$_['text_home']             = '<i class="fa fa-home"></i>';
$_['text_yes']              = 'Si';
$_['text_no']               = 'No';
$_['text_none']             = ' --- Nada --- ';
$_['text_select']           = ' --- Seleccione por favor --- ';
$_['text_all_zones']        = 'Todas las zonas';
$_['text_pagination']       = 'Viendo %d para %d de %d (%d páginas)';
$_['text_loading']          = 'Cargando...';

// Buttons
$_['button_address_add']    = 'Agregar dirección';
$_['button_back']           = 'Atrás';
$_['button_continue']       = 'Continuar';
$_['button_cart']           = 'Agregar al carro';
$_['button_cancel']         = 'Cancelar';
$_['button_compare']        = 'Comparar este producto';
$_['button_wishlist']       = 'Agregar a la Lista de Deseos';
$_['button_checkout']       = 'Pagar';
$_['button_confirm']        = 'Confirmar orden';
$_['button_coupon']         = 'Usar cupon';
$_['button_delete']         = 'Borrar';
$_['button_download']       = 'Descargar';
$_['button_edit']           = 'Editar';
$_['button_filter']         = 'Refinar búsqueda';
$_['button_new_address']    = 'Nueva dirección';
$_['button_change_address'] = 'Cambiar dirección';
$_['button_reviews']        = 'Opiniones';
$_['button_write']          = 'Escribir opinión';
$_['button_login']          = 'Login';
$_['button_update']         = 'Actualizar';
$_['button_remove']         = 'Quitar';
$_['button_reorder']        = 'Reordenar';
$_['button_return']         = 'Regresar';
$_['button_shopping']       = 'Continuar comprando';
$_['button_search']         = 'Buscar';
$_['button_shipping']       = 'Aplicar envio';
$_['button_submit']         = 'Enviar';
$_['button_guest']          = 'Comprar como invitado';
$_['button_view']           = 'Ver';
$_['button_voucher']        = 'Usar Vale';
$_['button_upload']         = 'Subir archivo';
$_['button_reward']         = 'Usar Puntos';
$_['button_quote']          = 'Obtener cotización';
$_['button_list']           = 'Lista';
$_['button_grid']           = 'Cuadrícula';
$_['button_map']            = 'Ver Google Map';

// Error
$_['error_exception']       = 'Código de error(%s): %s en %s en linea %s';
$_['error_upload_1']        = '¡Advertencia: El archivo subido excede la directiva de carga maxima del archivo en php.ini!';
$_['error_upload_2']        = '¡El archivo subido excede la directiva de MAX_FILE_SIZE para un formulario HTML!';
$_['error_upload_3']        = '¡Advertencia: El archivo subido se ha subido parcialmente!';
$_['error_upload_4']        = '¡Advertencia: El archivo no se ha subido!';
$_['error_upload_6']        = '¡Advertencia: Falta una carpeta temporal!';
$_['error_upload_7']        = '¡Advertencia: Fallo al escribir el archivo en disco!';
$_['error_upload_8']        = '¡Advertencia: carga de archivos se detuvo por extensión!';
$_['error_upload_999']      = '¡Advertencia: No hay código de error disponible!';
