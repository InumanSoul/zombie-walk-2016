<?php
// Text
$_['text_title']				= 'Cheque / Giro postal';
$_['text_instruction']			= 'Instrucciones para Cheque / Giro postal';
$_['text_payable']				= 'Hacer pagar a: ';
$_['text_address']				= 'Enviar a: ';
$_['text_payment']				= 'Su pedido no se enviará hasta que recibamos el pago.';
