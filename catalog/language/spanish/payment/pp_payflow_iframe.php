<?php
// Text
$_['text_title']				= 'Tarjeta de crédito o débito';
$_['text_secure_connection']	= 'Creando una conexión segura...';

// Error
$_['error_connection']			= 'No se pudo conectar a PayPal. Por favor contacte con el administrador de la tienda por ayuda o elija un método de pago diferente.';
