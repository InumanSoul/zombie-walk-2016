<?php
// Heading
$_['heading_title']				= 'Pagos Amazon';
$_['heading_address']			= 'Por favor, elija una dirección de entrega';
$_['heading_payment']			= 'Por favor elija un método de pago';
$_['heading_confirm']			= 'Resumen de pedido';

// Text
$_['text_back']					= 'Atrás';
$_['text_cart']					= 'Carro';
$_['text_confirm']				= 'Confirmar';
$_['text_continue']				= 'Continuar';
$_['text_cba']					= 'Pagos Amazon';
$_['text_enter_coupon']			= 'Ingrese su código de cupón aquí. Si no tiene uno, déjelo vacío.';
$_['text_coupon']				= 'Cupón';
$_['text_tax_other']			= 'Impuestos / Otros cargos de gestión';
$_['text_payment_failed']		= 'Su pago a fallado. Por favor contacte con el administrador de la tienda o use un aopción de pago diferente.';
$_['text_success_title']		= '¡Su pedido a sido ingresado!';
$_['text_payment_success']		= 'Su pedido se ingresó con éxito. Los detalles del pedido están debajo';

// Error
$_['error_payment_method']		= 'Por favor seleccione un método de pago';
$_['error_shipping']			= 'Por favor seleccione un método de envío';
$_['error_shipping_address']	= 'Por favor seleccione una dirección de entrega';
$_['error_shipping_methods']	= 'Se ha producido un error al recuperar su dirección de Amazon. Por favor contacte con el administrador de la tienda por ayuda';
$_['error_no_shipping_methods'] = 'No hay opciones de envío para la dirección seleccionada. Por favor seleccione una dirección de envío diferente';
