<?php
// Text
$_['text_title']				= 'Transferencia bancaria';
$_['text_instruction']			= 'Instrucciones para la transferencia';
$_['text_description']			= 'Por favor, transfiera el total del pago a la siguiente cuenta bancaria.';
$_['text_payment']				= 'Su pedido no será enviado hasta que se verifique el pago de la misma.';
