<?php

$_['text_title'] = 'Tarjeta de crédito / débito';
$_['text_card_details'] = 'Detalles de la tarjeta';
$_['text_wait'] = 'Procesando su pago';
$_['text_auth_code'] = 'Código de autorización: %s';
$_['text_postcode_check'] = 'Validación de código postal: %s';
$_['text_security_code_check'] = 'Validación CVV2: %s';
$_['text_address_check'] = 'Validación de la dirección: %s';
$_['text_3d_secure_check'] = '3D Secure: %s';
$_['text_not_given'] = 'No entregado';
$_['text_not_checked'] = 'No verificado';
$_['text_match'] = 'Coincidencia';
$_['text_not_match'] = 'No coincide';
$_['text_authenticated'] = 'Autenticado';
$_['text_not_authenticated'] = 'No autenticado';
$_['text_authentication_not_completed'] = 'Intentado pero no completado';
$_['text_unable_to_perform'] = 'No se puede realizar';
$_['text_transaction_declined'] = 'Su banco ha declinado la transacción. Por favor use un método de pago distinto.';
$_['text_transaction_failed'] = 'No se pudo procesar el pago. Por favor, consulte los datos que nos ha facilitado.';
$_['text_connection_error'] = 'Por favor, inténtelo de nuevo más tarde o use otro método de pago.';

$_['entry_type'] = "Tipo de tarjeta";
$_['entry_number'] = "Número de tarjeta";
$_['entry_expire_date'] = "Fecha de caducidad";
$_['entry_cvv2'] = "Código de seguridad (CVV2)";

$_['button_confirm'] = 'Confirmar';

$_['error_failure'] = 'No se pudo completar la transacción. Por favor, inténtelo de nuevo más tarde o use otro método de pago.';
