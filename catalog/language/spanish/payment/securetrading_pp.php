<?php

$_['text_title'] = 'Tarjeta de crédito / débito';
$_['button_confirm'] = 'Confirmar';

$_['text_postcode_check'] = 'Verificación de código postal: %s';
$_['text_security_code_check'] = 'Verificación CVV2: %s';
$_['text_address_check'] = 'Verificación de dirección: %s';
$_['text_not_given'] = 'No entregado';
$_['text_not_checked'] = 'No verificado';
$_['text_match'] = 'Coincidencia';
$_['text_not_match'] = 'No coincide';
$_['text_payment_details'] = 'Detalles del pago';

$_['entry_card_type'] = 'Tipo de tarjeta';
