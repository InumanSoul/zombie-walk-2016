<?php
// Text
$_['text_title']				= 'Tarjeta de crédito o débito (Procesado de manera segura por PayPal)';
$_['text_credit_card']			= 'Detalles de la tarjeta de crédito';
$_['text_start_date']			= '(si está disponible)';
$_['text_issue']				= '(solo para tarjetas Maestro y Solo)';
$_['text_wait']					= '¡Por favor espere!';

// Entry
$_['entry_cc_owner']			= 'Titular:';
$_['entry_cc_type']				= 'Tipo de tarjeta:';
$_['entry_cc_number']			= 'Número de tarjeta:';
$_['entry_cc_start_date']		= 'Tarjeta válida desde fecha:';
$_['entry_cc_expire_date']		= 'Fecha de expiración de la tarjeta:';
$_['entry_cc_cvv2']				= 'Código de seguridad de la tarjeta (CVV2):';
$_['entry_cc_issue']			= 'Número de emisión de la tarjeta:';

// Error
$_['error_required']			= 'Advertencia: Todos los campos de información de pago son requeridos.';
$_['error_general']				= 'Advertencia: Un problema general a ocurrido son la trasacción. Por favor intente de nuevo.';
$_['error_config']				= 'Advertencia: Error de configuración del módulo de pago. Por favor verifique las credenciales de acceso.';
$_['error_address']				= 'Advertencia: La ciudad de pago, estado, y código postal no coincide. Por favor intente de nuevo.';
$_['error_declined']			= 'Advertencia: Esta transacción ha sido denegada. Por favor intente de nuevo.';
$_['error_invalid']				= 'Advertencia: La información de la tarjeta de crédito es inválida. Por favor intente de nuevo.;
