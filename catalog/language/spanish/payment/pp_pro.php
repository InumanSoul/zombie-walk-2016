<?php
// Text
$_['text_title']				= 'Tarjeta de crédito o débito (Procesado de manera segura por PayPal)';
$_['text_wait']					= '¡Por favor espere!';
$_['text_credit_card']			= 'Detalles de la tarjeta de crédito';
$_['text_loading']				= 'Cargando';

// Entry
$_['entry_cc_type']				= 'Tipo de tarjeta';
$_['entry_cc_number']			= 'Número de tarjeta';
$_['entry_cc_start_date']		= 'Tarjeta válida desde fecha';
$_['entry_cc_expire_date']		= 'Fecha de expiración de la tarjeta';
$_['entry_cc_cvv2']				= 'Código de seguridad de la tarjeta (CVV2)';
$_['entry_cc_issue']			= 'Número de emisión de la tarjeta';

// Help
$_['help_start_date']			= '(si está disponible)';
$_['help_issue']				= '(solo para tarjetas Maestro y Solo)';
