<?php
// Heading
$_['express_text_title']		= 'Confirmar pedido';

// Text
$_['text_title']				= 'Pago exprés PayPal';
$_['button_continue']			= 'Continuar';
$_['text_cart']					= 'Carro de compras';
$_['text_shipping_updated']		= 'Servicio de envío actualizado';
$_['text_trial']				= '%s cada %s %s para %s pagos ';
$_['text_recurring']			= '%s cada %s %s';
$_['text_recurring_item']		= 'Item reiterado';
$_['text_length']				= 'para %s pagos';

// Entry
$_['express_entry_coupon']		= 'Ingrese su cupón aqui:';

// Button
$_['express_button_coupon']		= 'Agregar';
$_['express_button_confirm']	= 'Confirmar';
$_['express_button_login']		= 'Continuar a PayPal';
$_['express_button_shipping']	= 'Actualizar envío';
$_['button_cancel_recurring']	= 'Cancelar pagos';

// Error
$_['error_heading_title']		= 'Hubo un error';
$_['error_too_many_failures']	= 'Su pago ha fallado demasiadas veces';
