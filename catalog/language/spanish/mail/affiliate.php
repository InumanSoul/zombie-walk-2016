<?php
// Text
$_['text_subject']		        = '%s - Programa de Afiliados';
$_['text_welcome']		        = '¡Gracias por la afiliación a %s Programa de Afiliado!';
$_['text_login']                = 'Su cuenta ha sido creada y puede iniciar sesión utilizando su dirección de correo electrónico y una contraseña al visitar nuestro sitio web o en la siguiente URL:';
$_['text_approval']		        = 'Su cuenta debe ser aprobada antes de que pueda iniciar sesión. Una vez aprobada, usted puede iniciar sesión utilizando su dirección de correo electrónico y la contraseña al visitar nuestro sitio web o en la siguiente URL:';
$_['text_services']		        = 'Al iniciar la sesión, usted será capaz de generar códigos de seguimiento, seguir el pago de comisiones y editar la información de su cuenta.';
$_['text_thanks']		        = 'Gracias,';
$_['text_new_affiliate']        = 'Nuevo Afiliado';
$_['text_signup']		        = 'Un nuevo afiliado a firmado:';
$_['text_store']		        = 'Tienda:';
$_['text_firstname']	        = 'Primer Nombre:';
$_['text_lastname']		        = 'Apellido:';
$_['text_company']		        = 'Empresa:';
$_['text_email']		        = 'E-Mail:';
$_['text_telephone']	        = 'Teléfono:';
$_['text_website']		        = 'Sitio WEB:';
$_['text_order_id']             = 'ID de orden:';
$_['text_transaction_subject']  = '%s - Comisión de Afiliado';
$_['text_transaction_received'] = 'Usted a recibido %s de comisión!';
$_['text_transaction_total']    = 'El total de su comisión es ahora de %s.';
