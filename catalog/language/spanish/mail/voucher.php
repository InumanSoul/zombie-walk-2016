<?php
// Text
$_['text_subject']  = 'Se le ha enviado un vale de regalo de %s';
$_['text_greeting'] = 'Felicitaciones, usted ha recibido un vale de regalo %s';
$_['text_from']     = 'Este vale de regalo le ha sido enviado por %s';
$_['text_message']  = 'Con un mensaje que dice';
$_['text_redeem']   = 'Para canjear este vale de regalo, anote el código de canje que es <b>%s</b>, a continuación haga clic en el siguiente enlace y compre el producto que desee utilizando este vale de regalo. Puede introducir el código de vale de regalo en la página del carrito de compras antes de completar su pedido.';
$_['text_footer']   = 'Por favor responda a este correo electrónico si tiene alguna pregunta.';
