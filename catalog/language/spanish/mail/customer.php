<?php
// Text
$_['text_subject']        = '%s - Gracias por registrarse';
$_['text_welcome']        = '¡Bienvenido y gracias por registrarse a %s!';
$_['text_login']          = 'Su cuenta ha sido creada y puede iniciar sesión utilizando su dirección de correo electrónico y una contraseña al visitar nuestro sitio web o en la siguiente URL:';
$_['text_approval']       = 'Su cuenta debe ser aprobada antes de que pueda iniciar sesión. Una vez aprobada, usted puede iniciar sesión utilizando su dirección de correo electrónico y una contraseña al visitar nuestro sitio web o en la siguiente URL:';
$_['text_services']       = 'Al iniciar la sesión, usted podrá acceder a otros servicios, como la revisión de pedidos pasados, impresión de facturas y edición de la información de su cuenta.';
$_['text_thanks']         = 'Gracias,';
$_['text_new_customer']   = 'Nuevo Comprador';
$_['text_signup']         = 'Un nuevo comprador se a registrado:';
$_['text_website']        = 'Sitio WEB:';
$_['text_customer_group'] = 'Grupo de Compradores:';
$_['text_firstname']      = 'Nombre:';
$_['text_lastname']       = 'Apellido:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Teléfono:';
