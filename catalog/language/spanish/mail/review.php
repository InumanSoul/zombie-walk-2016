<?php
// Text
$_['text_subject']	= '%s - Reseña del producto';
$_['text_waiting']	= 'Usted tiene una nueva reseña del producto en espera.';
$_['text_product']	= 'Producto: %s';
$_['text_reviewer']	= 'Reseña: %s';
$_['text_rating']	= 'Calificación: %s';
$_['text_review']	= 'Información:';
