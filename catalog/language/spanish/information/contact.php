<?php
// Heading
$_['heading_title']  = 'Contáctenos';

// Text
$_['text_location']  = 'Nuestra ubicación';
$_['text_store']     = 'Nuestras Tiendas';
$_['text_contact']   = 'Formulario de contacto';
$_['text_address']   = 'Dirección';
$_['text_telephone'] = 'Teléfono';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Horarios';
$_['text_comment']   = 'Comentarios';
$_['text_success']   = '<p>¡Su consulta se ha enviado correctamente al dueño de la tienda!</p>';

// Entry
$_['entry_name']     = 'Nombre';
$_['entry_email']    = 'E-Mail';
$_['entry_enquiry']  = 'Consulta';
$_['entry_captcha']  = 'Ingrese el código';

// Email
$_['email_subject']  = 'Consulta %s';

// Errors
$_['error_name']     = '¡El nombre debe tener entre 3 y 32 caracteres!';
$_['error_email']    = '¡La dirección de email no parece ser válida!';
$_['error_enquiry']  = '¡La consulta de tener entre 10 y 3000 caracteres!';
$_['error_captcha']  = '¡El código de verificación no coincide con la imagen!';
