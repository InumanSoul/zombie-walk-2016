<?php
// Heading
$_['heading_title']        = 'Registrar cuenta';

// Text
$_['text_account']         = 'Cuenta';
$_['text_register']        = 'Registrar';
$_['text_account_already'] = 'Si usted ya posee una cuenta, por favor ingrese <a href="%s">Ingresar</a>.';
$_['text_your_details']    = 'Detalles personales';
$_['text_your_address']    = 'Su dirección';
$_['text_newsletter']      = 'Boletín';
$_['text_your_password']   = 'Su contraseña';
$_['text_agree']           = 'He leído y estoy de acuerdo con el <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Grupo de clientes';
$_['entry_firstname']      = 'Nombre';
$_['entry_lastname']       = 'Apellido';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Teléfono';
$_['entry_fax']            = 'Fax';
$_['entry_company']        = 'Empresa';
$_['entry_address_1']      = 'Direccion 1';
$_['entry_address_2']      = 'Direccion 2';
$_['entry_postcode']       = 'Código postal';
$_['entry_city']           = 'Ciudad';
$_['entry_country']        = 'Pais';
$_['entry_zone']           = 'Región / Estado';
$_['entry_newsletter']     = 'Suscribirse';
$_['entry_password']       = 'Contraseña';
$_['entry_confirm']        = 'Confirme contraseña';

// Error
$_['error_exists']         = '¡Advertencia: El e-mail ya está registrado!';
$_['error_firstname']      = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']       = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']          = '¡El e-mail no parece ser válido!';
$_['error_telephone']      = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_address_1']      = '¡La dirección 1 debe tener entre 3 y 128 caracteres!';
$_['error_city']           = '¡La ciudad debe tener entre 2 y 128 caracteres!';
$_['error_postcode']       = '¡El código postal debe tener entre 2 y 10 caracteres!';
$_['error_country']        = '¡Por favor, seleccione un pais!';
$_['error_zone']           = '¡Por favor seleccione una región / estado!';
$_['error_custom_field']   = '¡%s requerido!';
$_['error_password']       = '¡La contraseña debe tener entre 4 y 20 caracteres!';
$_['error_confirm']        = '¡La confirmación de la contraseña no coincide con la contraseña!';
$_['error_agree']          = '¡Advertencia: Usted debe aceptar los %s!';
