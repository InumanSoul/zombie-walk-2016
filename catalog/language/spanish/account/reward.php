<?php
// Heading
$_['heading_title']      = 'Puntos de recompensa';

// Column
$_['column_date_added']  = 'Fecha de alta';
$_['column_description'] = 'Descripción';
$_['column_points']      = 'Puntos';

// Text
$_['text_account']       = 'Cuenta';
$_['text_reward']        = 'Puntos de recompensa';
$_['text_total']         = 'Su total de puntos de recompensa es:';
$_['text_empty']         = '¡Usted no tiene ningún punto de recompensa!';
