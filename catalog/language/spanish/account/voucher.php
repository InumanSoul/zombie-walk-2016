<?php
// Heading
$_['heading_title']    = 'Compre un Vale de regalo';

// Text
$_['text_account']     = 'Cuenta';
$_['text_voucher']     = 'Vale de regalo';
$_['text_description'] = 'Este vale de regalo será enviado por e-mail al destinatario después de que su pedido haya sido pagado.';
$_['text_agree']       = 'Entiendo que los vales de regalo no son reembolsables.';
$_['text_message']     = '<p>¡Gracias por comprar un vale de regalo! Una vez que haya completado el pedido de su vale de regalo se enviará un correo electrónico con detalles sobre cómo canjearlo.</p>';
$_['text_for']         = '%s vale de regalo para %s';

// Entry
$_['entry_to_name']    = 'Destinatario';
$_['entry_to_email']   = 'E-mail del destinatario';
$_['entry_from_name']  = 'Su Nombre';
$_['entry_from_email'] = 'Su E-mail';
$_['entry_theme']      = 'Tema del vale de regalo';
$_['entry_message']    = 'Mensaje';
$_['entry_amount']     = 'Monto';

// Help
$_['help_message']     = 'Opcional';
$_['help_amount']      = 'El valor debe estar entre %s y %s';

// Error
$_['error_to_name']    = '¡El nombre del destinatario debe tener entre 1 y 64 caracteres!';
$_['error_from_name']  = '¡Su nombre debe tener entre 1 y 64 caracteres!';
$_['error_email']      = '¡El e-mail no parece ser válido!';
$_['error_theme']      = '¡Debe seleccionar un tema!';
$_['error_amount']     = '¡La cantidad debe estar entre %s y %s!';
$_['error_agree']      = '¡Advertencia: Debe estar de acuerdo en que los certificados de regalo no son reembolsables!';
