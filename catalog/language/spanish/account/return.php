<?php
// Heading
$_['heading_title']      = 'Devolución de Productos';

// Text
$_['text_account']       = 'Cuenta';
$_['text_return']        = 'Información de devolución';
$_['text_return_detail'] = 'Detalle de la devolución';
$_['text_description']   = 'Por favor complete el siguiente formulario para solicitar un número de RMA.';
$_['text_order']         = 'Información del pedido';
$_['text_product']       = 'Información del producto y razón de la devolución';
$_['text_message']       = '<p>Gracias por enviar su solicitud de devolución. Su solicitud ha sido enviada al departamento correspondiente para su procesamiento.</p><p> Se le notificará por correo electrónico sobre el estado de su solicitud.</p>';
$_['text_return_id']     = 'ID de devolución:';
$_['text_order_id']      = 'ID de pedido:';
$_['text_date_ordered']  = 'Fecha del pedido:';
$_['text_status']        = 'Estado:';
$_['text_date_added']    = 'Fecha de agregado:';
$_['text_comment']       = 'Comentarios de devolución';
$_['text_history']       = 'Historial de devoluciones';
$_['text_empty']         = '¡Usted no ha hecho ninguna devolución anterior!';
$_['text_agree']         = 'He leído y estoy de acuerdo con el <a href="%s" class="agree"><b>%s</b></a>';

// Column
$_['column_return_id']   = 'ID de devolucion';
$_['column_order_id']    = 'ID de pedido';
$_['column_status']      = 'Estado';
$_['column_date_added']  = 'Fecha de agregado';
$_['column_customer']    = 'Cliente';
$_['column_product']     = 'Nombre del producto';
$_['column_model']       = 'Modelo';
$_['column_quantity']    = 'Cantidad';
$_['column_price']       = 'Precio';
$_['column_opened']      = 'Iniciado';
$_['column_comment']     = 'Comentarios';
$_['column_reason']      = 'Motivo';
$_['column_action']      = 'Acción';

// Entry
$_['entry_order_id']     = 'ID de pedido';
$_['entry_date_ordered'] = 'Fecha del pedido';
$_['entry_firstname']    = 'Nombre';
$_['entry_lastname']     = 'Apellido';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Teléfono';
$_['entry_product']      = 'Nombre del producto';
$_['entry_model']        = 'Código del producto';
$_['entry_quantity']     = 'Cantidad';
$_['entry_reason']       = 'Motivo de devolución';
$_['entry_opened']       = 'El producto fue abierto';
$_['entry_fault_detail'] = 'Detalles defectuosos u otros';
$_['entry_captcha']      = 'Introduzca el código en la casilla de abajo';

// Error
$_['text_error']         = '¡La devolucion requerida no se pudo encontrar!';
$_['error_order_id']     = '¡Se requiere una ID de pedido!';
$_['error_firstname']    = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']     = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']        = '¡El e-mail no parece ser válido!';
$_['error_telephone']    = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_product']      = '¡El nombre del producto debe ser mayor a 3 y menor a 255 caracteres!';
$_['error_model']        = '¡El modelo del producto debe ser mayor a 3 y menor a 64 caracteres!';
$_['error_reason']       = '¡Debe seleccionar una razón de devolución!';
$_['error_captcha']      = '¡El código de verificación no coincide con la imagen!';
$_['error_agree']        = '¡Advertencia: Usted debe aceptar los %s!';
