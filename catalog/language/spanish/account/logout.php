<?php
// Heading
$_['heading_title'] = 'Salir de la cuenta';

// Text
$_['text_message']  = '<p>Ha salido de su cuenta. Ahora es seguro abandonar el ordenador.</p><p>Su carro de compras ha sido guardado y el contenido del mismo será restaurado cuando vuelva a entrar en su cuenta.</p>';
$_['text_account']  = 'Cuenta';
$_['text_logout']   = 'Salir';
