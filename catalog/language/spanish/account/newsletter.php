<?php
// Heading
$_['heading_title']    = 'Suscripción al boletín';

// Text
$_['text_account']     = 'Cuenta';
$_['text_newsletter']  = 'Boletín';
$_['text_success']     = '¡Exito: Su suscripción al boletín se ha actualizado correctamente!';

// Entry
$_['entry_newsletter'] = 'Suscribirse';
