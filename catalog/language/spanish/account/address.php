<?php
// Heading
$_['heading_title']        = 'Libreta de direcciones';

// Text
$_['text_account']         = 'Cuenta';
$_['text_address_book']    = 'Entradas en libreta de direcciones';
$_['text_edit_address']    = 'Editar dirección';
$_['text_add']             = 'Su dirección se ha insertado con éxito';
$_['text_edit']            = 'Su dirección se ha actualizado correctamente';
$_['text_delete']          = 'Su dirección se ha eliminado correctamente';
$_['text_empty']           = 'No tiene direcciones en su cuenta.';

// Entry
$_['entry_firstname']      = 'Nombre';
$_['entry_lastname']       = 'Apellido';
$_['entry_company']        = 'Empresa';
$_['entry_address_1']      = 'Dirección 1';
$_['entry_address_2']      = 'Dirección 2';
$_['entry_postcode']       = 'Código Postal';
$_['entry_city']           = 'Ciudad';
$_['entry_country']        = 'Pais';
$_['entry_zone']           = 'Región / Estado';
$_['entry_default']        = 'Dirección por defecto';

// Error
$_['error_delete']         = '¡Advertencia: Debe tener al menos una dirección!';
$_['error_default']        = '¡Advertencia: No puede eliminar su dirección por defecto!';
$_['error_firstname']      = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']       = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_vat']            = '¡El número de IVA no es inválido!';
$_['error_address_1']      = '¡La dirección debe tener entre 3 y 128 caracteres!';
$_['error_postcode']       = '¡El código postal debe tener entre 2 y 10 caracteres!';
$_['error_city']           = '¡La ciudad debe tener entre 2 y 128 caracteres!';
$_['error_country']        = '¡Por favor, seleccione un pais!';
$_['error_zone']           = '¡Por favor seleccione una región / estado!';
$_['error_custom_field']   = '¡%s requerido!';
