<?php
// Heading
$_['heading_title']   = '¿Olvidó su contraseña?';

// Text
$_['text_account']    = 'Cuenta';
$_['text_forgotten']  = 'Olvide mi contraseña';
$_['text_your_email'] = 'Dirección de e-mail';
$_['text_email']      = 'Introduzca la dirección de e-mail asociada a su cuenta. Presione el botón enviar y su contraseña será enviada por correo electrónico.';
$_['text_success']    = 'Exito: Una nueva contraseña ha sido enviada a su dirección de e-mail.';

// Entry
$_['entry_email']     = 'Dirección de e-mail';

// Error
$_['error_email']     = '¡Advertencia: La dirección de e-mail no se encuentra en nuestros registros, por favor, inténtelo de nuevo!';
