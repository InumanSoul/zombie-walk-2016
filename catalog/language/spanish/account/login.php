<?php
// Heading
$_['heading_title']                = 'Ingreso a la cuenta';

// Text
$_['text_account']                 = 'Cuenta';
$_['text_login']                   = 'Ingresar';
$_['text_new_customer']            = 'Crear Cuenta';
$_['text_register']                = 'Crear cuenta';
$_['text_register_account']        = 'Al crear una cuenta podrá realizar sus compras más rápidamente, revisar el estado de una orden, y realizar un seguimiento de los pedidos que ha hecho anteriormente.';
$_['text_returning_customer']      = 'Iniciar Sesión';
$_['text_i_am_returning_customer'] = 'Yo soy cliente que regresa';
$_['text_forgotten']               = 'Olvide mi contraseña';

// Entry
$_['entry_email']                  = 'Dirección de e-mail';
$_['entry_password']               = 'Contraseña';

// Error
$_['error_login']                  = '¡Advertencia: No hay resultados para la dirección de e-mail y/o contraseña!';
$_['error_attempts']               = '¡Advertencia: Su cuenta ha superado el número permitido de intentos de conexión. Inténtelo de nuevo en 1 hora!';
$_['error_approved']               = '¡Advertencia: Su cuenta requiere aprobación antes de que pueda iniciar sesión!';
