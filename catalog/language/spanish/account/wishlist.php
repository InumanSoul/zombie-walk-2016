<?php
// Heading
$_['heading_title'] = 'Mi lista de deseos';

// Text
$_['text_account']  = 'Cuenta';
$_['text_instock']  = 'Disponible';
$_['text_wishlist'] = 'Lista de deseos (%s)';
$_['text_login']    = '¡Usted debe <a href="%s">Ingresar</a> o <a href="%s">Registrarse</a> para guardar <a href="%s">%s</a> su <a href="%s">Lista de deseos</a>!';
$_['text_success']  = '¡Exito: a agregado <a href="%s">%s</a> a su <a href="%s">Lista de deseos</a>!';
$_['text_exists']   = '¡<a href="%s">%s</a> ya existe en su <a href="%s">Lista de deseos</a>!';
$_['text_remove']   = '¡Éxito: ha modificado su lista de deseos!';
$_['text_empty']    = '¡Su lista de deseos esta vacia!.';

// Column
$_['column_image']  = 'Imagen';
$_['column_name']   = 'Nombre del producto';
$_['column_model']  = 'Modelo';
$_['column_stock']  = 'Disponible';
$_['column_price']  = 'Precio unitario';
$_['column_action'] = 'Acción';
