<?php
// Heading
$_['heading_title'] = '¡Su cuenta ha sido creada!';

// Text
$_['text_message']  = '<p>¡Felicitaciones su nueva cuenta ha sido creada con éxito!</p> <p>Ahora puede disfrutar de los privilegios de una cuenta para mejorar su experiencia de compras en línea con nosotros.</p> <p>Si tiene alguna pregunta acerca del funcionamiento de esta tienda en línea, por favor envíe un correo electrónico al propietario de la tienda.</p> <p>Una confirmación ha sido enviado a la dirección de e-mail proporcionada. Si no lo ha recibido en una hora, por favor <a href="%s">contactenos</a>.</p>';
$_['text_approval'] = '<p>¡Gracias por registrarse con %s!</p><p>Se le notificará por e-mail una vez que su cuenta haya sido activada por el dueño de la tienda.</p><p>Si usted tiene alguna pregunta acerca del funcionamiento de esta tienda en línea, por favor <a href="%s">contactenos</a>.</p>';
$_['text_account']  = 'Cuenta';
$_['text_success']  = 'Exito';
