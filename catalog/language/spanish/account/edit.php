<?php
// Heading
$_['heading_title']      = 'Información de cuenta';

// Text
$_['text_account']       = 'Cuenta';
$_['text_edit']          = 'Editar información';
$_['text_your_details']  = 'Sus detalles personales';
$_['text_success']       = 'Exito: Su cuenta ha sido actualizada correctamente.';

// Entry
$_['entry_firstname']    = 'Nombre';
$_['entry_lastname']     = 'Apellido';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Teléfono';
$_['entry_fax']          = 'Fax';

// Error
$_['error_exists']       = '¡Advertencia: El e-mail ya está registrado!';
$_['error_firstname']    = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']     = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']        = '¡El e-mail no parece ser válido!';
$_['error_telephone']    = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_custom_field'] = '¡%s requerido!';
