<?php
// Heading
$_['heading_title']      = 'Mi Cuenta';

// Text
$_['text_account']       = 'Cuenta';
$_['text_my_account']    = 'Mi cuenta';
$_['text_my_orders']     = 'Mis pedidos';
$_['text_my_newsletter'] = 'Boletín';
$_['text_edit']          = 'Editar información de la cuenta';
$_['text_password']      = 'Cambiar contraseña';
$_['text_address']       = 'Modificar las entradas de la libreta de direcciones';
$_['text_wishlist']      = 'Modificar su lista de deseos';
$_['text_order']         = 'Ver historial de pedidos';
$_['text_download']      = 'Descargas';
$_['text_reward']        = 'Puntos de regalo';
$_['text_return']        = 'Solicitudes de devolución';
$_['text_transaction']   = 'Sus transacciones';
$_['text_newsletter']    = 'Suscribirse / darse de baja al boletín';
$_['text_recurring']     = 'Pagos recurrentes';
$_['text_transactions']  = 'Transacciones';
