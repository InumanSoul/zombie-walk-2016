<?php
// Heading
$_['heading_title']         = 'Historial de pedidos';

// Text
$_['text_account']          = 'Cuenta';
$_['text_order']            = 'Información de pedidos';
$_['text_order_detail']     = 'Detalles de pedidos';
$_['text_invoice_no']       = 'Factura No.:';
$_['text_order_id']         = 'ID de orden:';
$_['text_date_added']       = 'Fecha de alta:';
$_['text_shipping_address'] = 'Dirección de envio';
$_['text_shipping_method']  = 'Método de envio:';
$_['text_payment_address']  = 'Dirección de pago';
$_['text_payment_method']   = 'Método de pago:';
$_['text_comment']          = 'Comentarios del pedido';
$_['text_history']          = 'Historial de pedidos';
$_['text_success']          = '¡Exito: Ha añadido <a href="%s">%s</a> a su <a href="%s">carro de compras</a>!';
$_['text_empty']            = '¡Usted no ha hecho ningun pedido anterior!';
$_['text_error']            = '¡El pedido solicitado no se pudo encontrar!';

// Column
$_['column_order_id']       = 'ID de pedido';
$_['column_product']        = 'No. de producto';
$_['column_customer']       = 'Cliente';
$_['column_name']           = 'Nombre del producto';
$_['column_model']          = 'Modelo';
$_['column_quantity']       = 'Cantidad';
$_['column_price']          = 'Precio';
$_['column_total']          = 'Total';
$_['column_action']         = 'Acción';
$_['column_date_added']     = 'Fecha de agregado';
$_['column_status']         = 'Estado del pedido';
$_['column_comment']        = 'Comentarios';

// Error
$_['error_reorder']         = '%s actualmente no está disponible para ser reordenado.';
