<?php
// Text
$_['text_success']     = 'Exito: sus puntos de recompensa se han aplicado!';

// Error
$_['error_permission'] = '!Advertencia: Usted no tiene permiso para acceder a la API!';
$_['error_reward']     = '!Advertencia: Por favor ingrese la cantidad de puntos de recompensa para usar!';
$_['error_points']     = '!Advertencia: Usted no tiene %s puntos de recompensa!';
$_['error_maximum']    = '!Advertencia: El número máximo de puntos que se puede aplicar es %s!';
