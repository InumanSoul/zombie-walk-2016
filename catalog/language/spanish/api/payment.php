<?php
// Text
$_['text_address']       = '!Exito: La dirección de pago se ha fijado!';
$_['text_method']        = '!Éxito: La forma de pago se ha fijado!';

// Error
$_['error_permission']   = '!Advertencia: Usted no tiene permiso para acceder a la API!';
$_['error_firstname']    = '!El primer nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']     = '!El apellido deb tener entre 1 y 32 caracteres!';
$_['error_address_1']    = '!La dirección 1 debe tener entre 3 y 128 caracteres!';
$_['error_city']         = '!La ciudad debe tener entre 3 y 128 caracteres!';
$_['error_postcode']     = '!El código postal debe tener entre 2 y 10 caracteres para este país!';
$_['error_country']      = '!Por favor, seleccione un pais!';
$_['error_zone']         = '!Por favor seleccione una región / estado!';
$_['error_custom_field'] = '!%s requerido!';
$_['error_address']      = '!Advertencia: Dirección de pago requerida!';
$_['error_method']       = '!Advertencia: Metodo de pago requerido!';
$_['error_no_payment']   = '!Advertencia: No hay opciones de pago disponibles!';
