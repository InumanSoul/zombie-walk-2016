<?php
// Text
$_['text_success']     = 'Exito: Tu descuento vale de regalo se ha aplicado!';
$_['text_cart']        = 'Éxito: Ha modificado su carrito de compras!';

$_['text_for']         = '%s Vale de Regalo para %s';

// Error
$_['error_permission'] = '!Advertencia: Usted no tiene permiso para acceder a la API!';
$_['error_voucher']    = '!Advertencia: Vale de Regalo no es válido o el saldo se ha agotado!';
$_['error_to_name']    = '!El nombre del destinatario debe tener entre 1 y 64 caracteres!';
$_['error_from_name']  = '!Su nombre debe tener entre 1 y 64 caracteres!';
$_['error_email']      = '!La dirección de email no parece ser válida!';
$_['error_theme']      = '!Debe seleccionar un tema!';
$_['error_amount']     = '!La cantidad debe estar entre %s y %s!';
