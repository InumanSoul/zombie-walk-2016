<?php
// Text
$_['text_success']     = 'Exito: Ha modificado su carrito de compras!';

// Error
$_['error_permission'] = '!Advertencia: Usted no tiene permiso para acceder a la API!';
$_['error_stock']      = '!Los productos marcados con *** no están disponibles en la cantidad deseada o no estan en stock!';
$_['error_minimum']    = '!Cantidad de orden mínima para %s es %s!';
$_['error_store']      = '!El producto no se puede comprar en la tienda que a elegido!';
$_['error_required']   = '!%s requerido!';
