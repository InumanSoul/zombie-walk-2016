<?php
// Text
$_['text_success']       = 'Clientes se a modificado con éxito';

// Error
$_['error_permission']   = '!Advertencia: Usted no tiene permiso para acceder a la API!';
$_['error_firstname']    = '!El primer nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']     = '!El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']        = '!La dirección de correo electrónico no parece ser válida!';
$_['error_telephone']    = '!El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_custom_field'] = '!%s requerido!';
