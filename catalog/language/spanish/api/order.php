<?php
// Text
$_['text_success']           = 'Su pedido ha sido modificado correctamente';

// Error
$_['error_permission']       = '!Advertencia: Usted no tiene permiso para acceder a la API!';
$_['error_customer']         = '!Los datos del cliente deben ser establecidos!';
$_['error_payment_address']  = '!Dirección de pago requerida!';
$_['error_payment_method']   = '!Metodo de pago requerido!';
$_['error_shipping_address'] = '!Dirección de envio requerida!';
$_['error_shipping_method']  = '!Método de envio requerido!';
$_['error_stock']            = '!Los productos marcados con *** no esán disponibles en la cantidad deseada o no estan en stock!';
$_['error_minimum']          = '!Cantidad de orden mínima para %s es %s!';
$_['error_not_found']        = '!Advertencia: La orden no se pudo encontrar!';
