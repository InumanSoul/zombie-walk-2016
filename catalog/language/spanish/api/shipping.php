<?php
// Text
$_['text_address']       = '!Exito: La dirección de envío se ha establecido!';
$_['text_method']        = '!Exito: Método del envío se ha establecido!';

// Error
$_['error_permission']   = '!Advertencia: Usted no tiene permiso para acceder a la API!';
$_['error_firstname']    = '!El primer nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']     = '!El apellido debe tener entre 1 y 32 caracteres!';
$_['error_address_1']    = '!La dirección 1 debe tener entre 3 y 128 caracteres!';
$_['error_city']         = '!La ciudad debe tener entre 3 y 128 caracteres!';
$_['error_postcode']     = '!El código postal debe tener entre 2 y 10 caracteres para este pais!';
$_['error_country']      = '!Por favor, seleccione un pais!';
$_['error_zone']         = '!Por favor seleccione una región / estado!';
$_['error_custom_field'] = '!%s requerido!';
$_['error_address']      = '!Advertencia: Dirección de envío necesaria!';
$_['error_method']       = '!Advertencia: Método del envío necesario!';
$_['error_no_shipping']  = '!Advertencia: No hay opciones de envío disponibles!';
