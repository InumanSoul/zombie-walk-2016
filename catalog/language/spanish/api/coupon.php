<?php
// Text
$_['text_success']     = '!Exito: Su cupon de descuento se ha aplicado!';

// Error
$_['error_permission'] = '!Advertencia: Usted no tiene permiso para acceder a la API!';
$_['error_coupon']     = '!Advertencia: El cupon no es válido, esta vencido o alcanzó el limite de uso!';
