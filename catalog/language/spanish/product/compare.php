<?php
// Heading
$_['heading_title']     = 'Comparación de Producto';

// Text
$_['text_product']      = 'Detalle del Producto';
$_['text_name']         = 'Producto';
$_['text_image']        = 'Imagen';
$_['text_price']        = 'Precio';
$_['text_model']        = 'Modelo';
$_['text_manufacturer'] = 'Marca';
$_['text_availability'] = 'Disponibilidad';
$_['text_instock']      = 'Disponible';
$_['text_rating']       = 'Clasificación';
$_['text_reviews']      = 'Basado en %s opiniones.';
$_['text_summary']      = 'Sumario';
$_['text_weight']       = 'Peso';
$_['text_dimension']    = 'Dimensiones (L x P x A)';
$_['text_compare']      = 'Producto Comparado (%s)';
$_['text_success']      = '¡Exito: Ha añadido <a href="%s">%s</a> a su <a href="%s">Comparación de Productos</a>!';
$_['text_remove']       = '¡Exito: Ha modificado su comparación de productos!';
$_['text_empty']        = 'No ha seleccionado ningun producto a comparar.';
