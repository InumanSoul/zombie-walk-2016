<?php
// Text
$_['text_search']                             = 'Buscar';
$_['text_brand']                              = 'Marca';
$_['text_manufacturer']                       = 'Marca:';
$_['text_model']                              = 'Código de Producto:';
$_['text_reward']                             = 'Puntos de recompensa:';
$_['text_points']                             = 'Precio en puntos de recompensa:';
$_['text_stock']                              = 'Disponibles:';
$_['text_instock']                            = 'Disponible';
$_['text_tax']                                = 'Sin impuesto:';
$_['text_discount']                           = ' o más ';
$_['text_option']                             = 'Opciones disponibles';
$_['text_minimum']                            = 'Este producto tiene una cantidad mínima de %s';
$_['text_reviews']                            = '%s opiniones';
$_['text_write']                              = 'Escriba una opinión';
$_['text_login']                              = 'Por favor <a href="%s">Accede</a> o <a href="%s">Registrate</a> para opinar';
$_['text_no_reviews']                         = 'No hay opiniones sobre este producto.';
$_['text_note']                               = '¡<span class="text-danger">Nota:</span> HTML no traducido!';
$_['text_success']                            = 'Gracias por su opinión. Fue enviada al administrador para su aprobación.';
$_['text_related']                            = 'Productos relacionados';
$_['text_tags']                               = 'Etiquetas:';
$_['text_error']                              = '¡Producto no encontrado!';
$_['text_payment_recurring']                    = 'Perfiles de Pago';
$_['text_trial_description']                  = '%s cada %d %s(s) para %d pago(s)';
$_['text_payment_description']                = '%s cada %d %s(s) para %d pago(s)';
$_['text_payment_until_canceled_description'] = '%s cada %d %s(s) hasta que se cancele';
$_['text_day']                                = 'dia';
$_['text_week']                               = 'semana';
$_['text_semi_month']                         = 'medio mes';
$_['text_month']                              = 'mes';
$_['text_year']                               = 'año';

// Entry
$_['entry_qty']                               = 'Cantidad';
$_['entry_name']                              = 'Su nombre';
$_['entry_review']                            = 'Su opinión';
$_['entry_rating']                            = 'Clasificación';
$_['entry_good']                              = 'Bueno';
$_['entry_bad']                               = 'Malo';
$_['entry_captcha']                           = 'Ingrese el codigo mostrado en la caja';

// Tabs
$_['tab_description']                         = 'Descripción';
$_['tab_attribute']                           = 'Especificaciones';
$_['tab_review']                              = 'Opiniones (%s)';

// Error
$_['error_name']                              = '¡Advertencia: El nombre debe tener entre 3 y 25 caracteres!';
$_['error_text']                              = '¡Advertencia: La opinión debe tener entre 3 y 1000 caracteres!';
$_['error_rating']                            = '¡Advertencia: Por favor, seleccione la clasificación a otorgar!';
$_['error_captcha']                           = '¡Advertencia: Código de verificación no coincide con la imagen!';
