<?php
// Text
$_['text_refine']       = 'Busqueda refinada';
$_['text_product']      = 'Productos';
$_['text_error']        = '¡Categoría no encontrada!';
$_['text_empty']        = 'No hay productos en esta categoría.';
$_['text_quantity']     = 'Cantidad:';
$_['text_manufacturer'] = 'Marca:';
$_['text_model']        = 'Código de Producto:';
$_['text_points']       = 'Puntos de recompensa:';
$_['text_price']        = 'Precio:';
$_['text_tax']          = 'Sin impuesto:';
$_['text_compare']      = 'Producto a comparar(%s)';
$_['text_sort']         = 'Ordenar por:';
$_['text_default']      = 'Por defecto';
$_['text_name_asc']     = 'Nombre (A - Z)';
$_['text_name_desc']    = 'Nombre (Z - A)';
$_['text_price_asc']    = 'Precio (Menor a Mayor)';
$_['text_price_desc']   = 'Precio (Mayor a Menor)';
$_['text_rating_asc']   = 'Clasificación (Menor)';
$_['text_rating_desc']  = 'Clasificación (Mayor)';
$_['text_model_asc']    = 'Modelo (A - Z)';
$_['text_model_desc']   = 'Modelo (Z - A)';
$_['text_limit']        = 'Mostrar:';
