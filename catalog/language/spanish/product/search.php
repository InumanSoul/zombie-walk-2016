<?php
// Heading
$_['heading_title']     = 'Buscar';
$_['heading_tag']		= 'Etiqueta - ';

// Text
$_['text_search']       = 'Productos que satisfacen los criterios de búsqueda';
$_['text_keyword']      = 'Palabras clave';
$_['text_category']     = 'Todas las categorías';
$_['text_sub_category'] = 'Buscar en sub-categorías';
$_['text_empty']        = 'No hay productos que correspondan con los criterios de búsqueda.';
$_['text_quantity']     = 'Cantidad:';
$_['text_manufacturer'] = 'Marca:';
$_['text_model']        = 'Código de Producto:';
$_['text_points']       = 'Puntos de Recompensa:';
$_['text_price']        = 'Precio:';
$_['text_tax']          = 'Sin impuesto:';
$_['text_reviews']      = 'Basado en %s opiniones.';
$_['text_compare']      = 'Producto de comparación (%s)';
$_['text_sort']         = 'Ordenar por:';
$_['text_default']      = 'Por defecto';
$_['text_name_asc']     = 'Nombre (A - Z)';
$_['text_name_desc']    = 'Nombre (Z - A)';
$_['text_price_asc']    = 'Precio (Menor a Mayor)';
$_['text_price_desc']   = 'Precio (Mayor a Menor)';
$_['text_rating_asc']   = 'Clasificacion (Menor)';
$_['text_rating_desc']  = 'Clasificacion (Mayor)';
$_['text_model_asc']    = 'Modelo (A - Z)';
$_['text_model_desc']   = 'Modelo (Z - A)';
$_['text_limit']        = 'Mostrar:';

// Entry
$_['entry_search']      = 'Criterio de búsqueda';
$_['entry_description'] = 'Buscar en descripciones de productos';
