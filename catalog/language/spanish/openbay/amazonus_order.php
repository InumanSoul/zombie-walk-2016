<?php
// Text
$_['text_paid_amazon'] 			= 'Pagado en Amazon US';
$_['text_total_shipping'] 		= 'Envío';
$_['text_total_shipping_tax'] 	= 'Cargo de envío';
$_['text_total_giftwrap'] 		= 'Papel de regalo';
$_['text_total_giftwrap_tax'] 	= 'Cargo por papel de regalo';
$_['text_total_sub'] 			= 'Sub-total';
$_['text_tax'] 					= 'Impuesto';
$_['text_total'] 				= 'Total';
