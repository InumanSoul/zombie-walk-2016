<?php
// Heading
$_['heading_title']        = '¡Su pedido ha sido realizado!';

// Text
$_['text_basket']          = 'Carros de compras';
$_['text_checkout']        = 'Pago';
$_['text_success']         = 'Exito';
$_['text_customer']        = '<p>¡Su pedido ha sido procesado correctamente!</p><p>Puede ver su historial de pedidos ingresando a <a href="%s">mi cuenta</a> y haciendo clic en <a href="%s">historial</a>.</p><p>Si su compra tiene una descarga asociada, se puede ir a la página <a href="%s">descargas</a> para verla.</p><p>Por favor, dirija cualquier pregunta que tenga al <a href="%s">dueño de la tienda</a>.</p><p>Gracias por hacer compras con nosotros en línea!</p>';
$_['text_guest']           = '<p>¡Su pedido ha sido procesado correctamente!</p><p>Por favor, dirija cualquier pregunta que tenga al <a href="%s">dueño de la tienda</a>.</p><p>Gracias por hacer compras con nosotros en línea!</p>';
