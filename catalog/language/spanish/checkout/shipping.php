<?php
// Heading
$_['heading_title']        = 'Costos estimados de envio';

// Text
$_['text_success']         = '¡Exito: Su precio estimado de envío se ha aplicado!';
$_['text_shipping']        = 'Ingrese su destino para obtener un precio estimado de envío.';
$_['text_shipping_method'] = 'Por favor, seleccione el método de envío preferido para usar en esta orden.';

// Entry
$_['entry_country']        = 'Pais';
$_['entry_zone']           = 'Región o Estado';
$_['entry_postcode']       = 'Código Postal';

// Error
$_['error_postcode']       = '¡El código postal debe tener entre 2 y 10 caracteres!';
$_['error_country']        = '¡Por favor, seleccione un pais!';
$_['error_zone']           = '¡Por favor seleccione una región / estado!';
$_['error_shipping']       = '¡Advertencia: Método del envío necesario!';
$_['error_no_shipping']    = '¡Advertencia: No hay opciones de envío disponibles. Por Favor <a href="%s">contactenos</a> para asistencia!';
