<?php
// Heading
$_['heading_title']    = 'Carro de Compras';

// Text
$_['text_success']     = '¡Exito: Has añadido <a href="%s">%s</a> a tu <a href="%s">carro de compras</a>!';
$_['text_remove']      = '¡Exito: has modificado tu carro de compras!';
$_['text_login']       = '¡Atención: Usted debe <a href="%s">ingresar</a> o <a href="%s">Registrarse</a> para ver los precios!';
$_['text_items']       = '%s articulo(s) - %s';
$_['text_points']      = 'Puntos de regalo: %s';
$_['text_next']        = '¿Que le gustaría hacer a continuación?';
$_['text_next_choice'] = 'Elija si tiene un código de descuento o de puntos de reconpensa que desea utilizar o desea estimar su coste de entrega.';
$_['text_empty']       = '!Su carro de compras esta vacio!';
$_['text_day']         = 'Dia';
$_['text_week']        = 'Semana';
$_['text_semi_month']  = 'Quincena';
$_['text_month']       = 'Mes';
$_['text_year']        = 'Año';
$_['text_trial']       = '%s cada %s %s para %s pagos después';
$_['text_recurring']   = '%s cada %s %s';
$_['text_length']      = ' para %s pagos';
$_['text_until_cancelled']   	= 'Hasta que se cancele';
$_['text_recurring_item']    	              = 'Elemento repetido';
$_['text_payment_recurring']                    = 'Perfil de pago';
$_['text_trial_description'] 	              = '%s cada %d %s(s) para %d pagos(s)';
$_['text_payment_description'] 	              = '%s cada %d %s(s) para %d pago(s)';
$_['text_payment_until_canceled_description'] = '%s cada %d %s(s) hasta que se cancele';

// Column
$_['column_image']          = 'Imagen';
$_['column_name']           = 'Nombre del Producto';
$_['column_model']          = 'Modelo';
$_['column_quantity']       = 'Cantidad';
$_['column_price']          = 'Precio Unitario';
$_['column_total']          = 'Total';

// Error
$_['error_stock']            = '¡Los productos marcados con *** no están disponibles en la cantidad deseada o no estan en stock!';
$_['error_minimum']          = '¡Cantidad de orden mínima para %s es %s!';
$_['error_required']         = '¡%s requerido!';
$_['error_product']          = '¡Advertencia: No hay artículos en el carro de compras!';
$_['error_recurring_required'] = '¡Seleccione un pago periodico!';
