<?php
// Heading
$_['heading_title'] = 'Usar vale de regalo';

// Text
$_['text_success']  = '¡Exito: su vale de regalo se ha aplicado!';

// Entry
$_['entry_voucher'] = 'Ingrese su código de vale de regalo aquí';

// Error
$_['error_voucher'] = '¡Advertencia: El vale de regalo no es válido o el saldo se ha agotado!';
$_['error_empty']   = '¡Advertencia: Por favor, introduzca un código de vale!';
