<?php
// Heading
$_['heading_title'] = 'Utilice puntos de recompensa (Disponible %s)';

// Text
$_['text_success']  = '¡Exito: el descuento de sus puntos de recompensa se ha aplicado!';

// Entry
$_['entry_reward']  = 'Puntos a usar (Max %s)';

// Error
$_['error_reward']  = '¡Advertencia: Por favor ingrese la cantidad de puntos de recompensa a usar!';
$_['error_points']  = '¡Advertencia: Usted no tiene %s puntos de recompensa!';
$_['error_maximum'] = '¡Advertencia: El número máximo de puntos que se puede usar es %s!';
