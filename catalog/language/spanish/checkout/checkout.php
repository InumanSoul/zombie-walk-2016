<?php
// Heading
$_['heading_title']                  = 'Pagar';

// Text
$_['text_cart']                      = 'Carro de compras';
$_['text_checkout_option']           = 'Paso 1: Opciones de pago';
$_['text_checkout_account']          = 'Paso 2: Cuenta y detalles de facturación';
$_['text_checkout_payment_address']  = 'Paso 2: Detalles de facturación';
$_['text_checkout_shipping_address'] = 'Paso 3: Detalles para envio';
$_['text_checkout_shipping_method']  = 'Paso 4: Metodo de envio';
$_['text_checkout_payment_method']   = 'Paso 5: Metodo de pago';
$_['text_checkout_confirm']          = 'Paso 6: Confirmar orden';
$_['text_modify']                    = 'Modificar &raquo;';
$_['text_new_customer']              = 'Nuevo cliente';
$_['text_returning_customer']        = 'Cliente recurrente';
$_['text_checkout']                  = 'Opciones de pago:';
$_['text_i_am_returning_customer']   = 'Soy un cliente que regresa';
$_['text_register']                  = 'Registrarse';
$_['text_guest']                     = 'Pedido Invitado';
$_['text_register_account']          = 'Al crear una cuenta podrá realizar sus compras rápidamente, revisar el estado de la orden, y realizar un seguimiento de los pedidos que ha hecho anteriormente.';
$_['text_forgotten']                 = '¿Olvido su contraseña?';
$_['text_your_details']              = 'Informacion Personal';
$_['text_your_address']              = 'Su dirección';
$_['text_your_password']             = 'Su contraseña';
$_['text_agree']                     = 'He leído y estoy de acuerdo con el <a href="%s" class="Acepto"><b>%s</b></a>';
$_['text_address_new']               = 'Deseo usar una nueva dirección';
$_['text_address_existing']                   = 'Deseo usar una direccion existente';
$_['text_shipping_method']                    = 'Por favor, seleccione el método de envío preferido para usar en esta orden.';
$_['text_payment_method']                     = 'Por favor, seleccione el método de pago preferido para usar en esta orden.';
$_['text_comments']                           = 'Agregar comentarios a su orden';
$_['text_recurring']                          = 'Articulo repetido';
$_['text_payment_recurring']                  = 'Perfiles de pago';
$_['text_trial_description']                  = '%s cada %d %s(s) para %d pago(s)';
$_['text_payment_description']                = '%s cada %d %s(s) para %d pago(s)';
$_['text_payment_until_canceled_description'] = '%s cada %d %s(s) hasta que se cancele';
$_['text_day']                       = 'Dia';
$_['text_week']                      = 'Semana';
$_['text_semi_month']                = 'Quincena';
$_['text_month']                     = 'Mes';
$_['text_year']                      = 'Año';

// Column
$_['column_name']                    = 'Nombre del Producto';
$_['column_model']                   = 'Modelo';
$_['column_quantity']                = 'Cantidad';
$_['column_price']                   = 'Precio Unitario';
$_['column_total']                   = 'Total';

// Entry
$_['entry_email_address']            = 'Dirección de email';
$_['entry_email']                    = 'Email';
$_['entry_password']                 = 'Contraseña';
$_['entry_confirm']                  = 'Confirmar contraseña';
$_['entry_firstname']                = 'Nombre';
$_['entry_lastname']                 = 'Apellido';
$_['entry_telephone']                = 'Teléfono';
$_['entry_fax']                      = 'Fax';
$_['entry_address']                  = 'Dirección';
$_['entry_company']                  = 'Empresa';
$_['entry_customer_group']           = 'Grupo de compradores';
$_['entry_address_1']                = 'Dirección 1';
$_['entry_address_2']                = 'Dirección 2';
$_['entry_postcode']                 = 'Código Postal';
$_['entry_city']                     = 'Ciudad';
$_['entry_country']                  = 'Pais';
$_['entry_zone']                     = 'Región / Estado';
$_['entry_newsletter']               = 'Deseo suscribirme a %s boletin.';
$_['entry_shipping'] 	             = 'Mis direcciones de entrega y facturación son las mismas.';

// Error
$_['error_warning']                  = '¡Hubo un problema al intentar procesar su orden! Si el problema persiste por favor trate de seleccionar un método de pago diferente o puede ponerse en contacto con el dueño de la tienda de<a href="%s">Click aqui</a>.';
$_['error_login']                    = '¡Advertencia: No hay resultados para la dirección de email y / o contraseña!';
$_['error_attempts']                 = '¡Advertencia: Tu cuenta ha superado el número permitido de intentos de conexión. Inténtalo de nuevo en 1 hora!';
$_['error_approved']                 = '¡Atención: Su cuenta requiere aprobación antes de que pueda iniciar sesión!';
$_['error_exists']                   = '¡Advertencia: El email ya está registrado!';
$_['error_firstname']                = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']                 = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']                    = '¡La dirección de email no parece ser válida!';
$_['error_telephone']                = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_password']                 = '¡La contraseña debe tener entre 3 y 20 caracteres!';
$_['error_confirm']                  = '¡La confirmación de la contraseña no coincide con la contraseña!';
$_['error_address_1']                = '¡La dirección 1 debe tener entre 3 y 128 caracteres!';
$_['error_city']                     = '¡La ciudad debe tener entre 2 y 128 caracteres!';
$_['error_postcode']                 = '¡El código postal debe tener entre 2 y 10 caracteres!';
$_['error_country']                  = '¡Por favor, seleccione un pais!';
$_['error_zone']                     = '¡Por favor seleccione una región / estado!';
$_['error_agree']                    = '¡Advertencia: Usted debe aceptar los %s!';
$_['error_address']                  = '¡Advertencia: Debe seleccionar una dirección!';
$_['error_shipping']                 = '¡Advertencia: Método del envío necesario!';
$_['error_no_shipping']              = '¡Advertencia: No hay opciones de envío disponibles. Por Favor <a href="%s">contactenos</a> para asistencia!';
$_['error_payment']                  = '¡Advertencia: Forma de pago requida!';
$_['error_no_payment']               = '¡Advertencia: No hay opciones de pago disponibles. Por Favor<a href="%s">contactenos</a> para asistencia!';
$_['error_custom_field']             = '%s requerido!';
