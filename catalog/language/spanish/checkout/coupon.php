<?php
// Heading
$_['heading_title'] = 'Usar código de cupón de descuento';

// Text
$_['text_success']  = '¡Exito: Su cupón de descuento se ha aplicado!';

// Entry
$_['entry_coupon']  = 'Ingrese su cupón aquí';

// Error
$_['error_coupon']  = '¡Advertencia: El cupón no es válido, está vencido o a alcanzado su límite de uso!';
$_['error_empty']   = '¡Advertencia: Por favor, introduzca un código de cupón!';
