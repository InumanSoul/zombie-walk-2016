<?php
// Heading
$_['heading_title'] = '¡Error en el pago!';

// Text
$_['text_basket']   = 'Carro de compra';
$_['text_checkout'] = 'Pago';
$_['text_failure']  = 'Error de pago';
$_['text_message']  = '<p>Hubo un problema al procesar su pago y la orden no se ha completado.</p>

<p>Posibles razones:</p>
<ul>
  <li>Fondos insuficientes</li>
  <li>Verificación fallida</li>
</ul>

<p>Por favor trate de ordenar de nuevo utilizando otro método de pago.</p>

<p>Si el problema persiste por favor <a href="%s">contactenos</a> con los detalles de la orden que usted está tratando de ingresar.</p>';
