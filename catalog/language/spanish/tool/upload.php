<?php
// Text
$_['text_upload']    = '¡Su archivo se subió satisfactoriamente!';

// Error
$_['error_filename'] = '¡El nombre del archivo debe tener entre 3 y 64 caracteres!';
$_['error_filetype'] = '¡Extensión de archivo invalido!';
$_['error_upload']   = '¡Subida requerida!';
