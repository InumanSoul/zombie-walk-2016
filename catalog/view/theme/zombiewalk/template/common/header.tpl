<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="generator" content="Atom">
<meta name="msapplication-TileColor" content="#C0CA33">
<meta name="theme-color" content="#C0CA33">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/materialize/css/materialize.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/materialize/js/materialize.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!--Fuentes Google-->
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,400italic,300italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:700,400' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="catalog/view/theme/zombiewalk/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/script-zw.js" type="text/javascript"></script>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<div id="st-container" class="st-container st-effect-1">

<!--Dropdowns init-->
<ul id="idiomas" class="dropdown-content">
    <?php echo $language; ?>
</ul>
<ul id="mobile-dropdown" class="dropdown-content">
  <?php if ($logged) { ?>
  <li><a href="<?php echo $account; ?>" class="waves-effect"><i class="material-icons left">account_circle</i> <?php echo $customer_name; ?></a></li>
  <li class="divider"></li>
  <li><a href="<?php echo $account; ?>" class="waves-effect"><i class="material-icons left">dashboard</i> <?php echo $text_account; ?></a></li>
  <?php echo $language; ?>
  <li><a href="<?php echo $logout; ?>" class="waves-effect"><i class="material-icons left">exit_to_app</i> <?php echo $text_logout; ?></a></li>
  <?php } else { ?>
  <li><a href="<?php echo $login; ?>" class="waves-effect"><?php echo $text_login; ?></a></li>
  <?php echo $language; ?>
  <?php } ?>
</ul>
<ul id="account-dp" class="dropdown-content">
  <?php if ($logged) { ?>
  <li><a href="<?php echo $account; ?>" class="waves-effect"><i class="material-icons left">account_circle</i> <?php echo $text_account; ?></a></li>
  <li><a href="<?php echo $logout; ?>" class="waves-effect"><i class="material-icons left">exit_to_app</i> <?php echo $text_logout; ?></a></li>
  <?php } else { ?>
  <li><a href="<?php echo $login; ?>" class="waves-effect"><?php echo $text_login; ?></a></li>
  <?php } ?>
</ul>
<!--Finish Dropdown-->
<div class="toggle-nav hide-on-large-only">
  <div id="nav-icon4">
    <span></span>
    <span></span>
    <span></span>
  </div>
</div>

<div class="navbar-fixed">
  <nav class="nav-zombie">
    <div class="nav-wrapper">

      <a href="#!" class="brand-logo center">Zombie Walk</a>
      <ul class="left hide-on-med-and-down">
        <li><a href="<?php echo $home; ?>" class="waves-effect"><?php echo $text_home; ?></a></li>
        <li><a href="#" class="waves-effect"><?php echo $text_map; ?></a></li>
        <li><a href="#" class="waves-effect"><?php echo $text_rules; ?></a></li>
        <li><a href="#" class="waves-effect"><?php echo $text_gallery; ?></a></li>
        <li><a href="#" class="waves-effect"><?php echo $text_shop; ?></a></li>
      </ul>
      <!-- Dropdown Mobile -->
      <div class="ac-nav hide-on-large-only">
          <div class="dropdown-button waves-effect" href="#!" data-activates="mobile-dropdown">
            <i class="material-icons right">more_vert</i>
          </div>
      </div>
      <div id="nav">
       <ul class="right hide-on-med-and-down">
        <?php if ($logged) { ?>
        <li><a href="#!" data-activates="account-dp" class="dropdown-button waves-effect"><?php echo $customer_name; ?></a></li>
        <?php } else { ?>
        <li><a href="<?php echo $login; ?>" class="waves-effect"><?php echo $text_login; ?></a></li>
        <?php } ?>
       <!-- Dropdown Idioma -->
        <li><a class="dropdown-button waves-effect" href="#!" data-activates="idiomas"><?php echo $text_language; ?><i class="material-icons right">arrow_drop_down</i></a></li>
      </ul>
     </div>
    </div>
  </nav>
</div>
<nav class="st-menu st-effect-1" id="menu-1">
	<img src="catalog/view/theme/zombiewalk/image/menu.png" class="responsive-img"/>
	<ul>
	    <li><a href="<?php echo $home; ?>" class="waves-effect"><?php echo $text_home; ?></a></li>
	    <li><a href="#" class="waves-effect"><?php echo $text_map; ?></a></li>
	    <li><a href="#" class="waves-effect"><?php echo $text_rules; ?></a></li>
	    <li><a href="#" class="waves-effect"><?php echo $text_gallery; ?></a></li>
	    <li><a href="#" class="waves-effect"><?php echo $text_shop; ?></a></li>
	</ul>
</nav>


  <div class="fixed-action-btn click-to-toggle" style="bottom: 40px; right: 25px;">
    <a id="fab" class="btn-floating btn-large waves-effect red">
      <i id="add-btn" class="large material-icons">add</i>
    </a>
    <ul>
      <li><a class="btn-floating tooltipped green waves-effect" data-position="left" data-delay="50" data-tooltip="Confirmar" href="<?php echo $checkout; ?>"><i class="material-icons">done_all</i></a></li>
      <li><a class="btn-floating tooltipped red waves-effect" data-position="left" data-delay="50" data-tooltip="Favoritos" href="<?php echo $wishlist; ?>"><i class="material-icons">favorite</i></a></li>
      <li><a class="btn-floating tooltipped blue waves-effect" data-position="left" data-delay="50" data-tooltip="Carrito" href="<?php echo $shopping_cart; ?>"><i class="material-icons">shopping_cart</i></a></li>
    </ul>
  </div>
